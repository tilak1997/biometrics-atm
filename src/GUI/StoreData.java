package GUI;

import Algorithm.*;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Algorithm.CFingerPrint;
import MFS100.DeviceInfo;
import MFS100.FingerData;
import MFS100.MFS100;
import MFS100.MFS100Event;

public class StoreData extends JFrame implements ActionListener,MFS100Event {

	private JPanel contentPane;
	
	private JTextField txtACC_ID;
	private JTextField txtACC_TYPE;
	private JTextField txtC_ID;
	private JTextField txtBALANCE;
	private JTextField txtCARD_NUMBER;
	private JTextField txtPIN_NO;
	
	private BJPanel lbl_fingerprint_image;
	private JTextField txtfinger;
	
	JButton btnFINGER,btncancel,btnsave,btnUpdate;
	
	String acc_id="",acc_type="",c_id="";
	String balance="",card_no="",pin_no="";
	String fingerprint="";
	byte[] fingerprint_image = null;
//	BufferedImage icon;
	
	CFingerPrint m_finger1 = new CFingerPrint();
	BufferedImage icon = new BufferedImage(m_finger1.FP_IMAGE_WIDTH ,m_finger1.FP_IMAGE_HEIGHT,BufferedImage.TYPE_INT_RGB );

	private BufferedImage m_bimage1 = new BufferedImage(m_finger1.FP_IMAGE_WIDTH ,m_finger1.FP_IMAGE_HEIGHT,BufferedImage.TYPE_INT_RGB );
	double finger1[] = new double[m_finger1.FP_TEMPLATE_MAX_SIZE];
	
	ConnectionClass connection = new ConnectionClass();
	Connection con = null;
	
	//fingerprint device components
	MFS100 mfs100= null; 
	String Key = "";
	DeviceInfo deviceInfo = null;
	int ret;
	
	class BJPanel extends JPanel 
	  {
	    public BufferedImage bi;
	    public BJPanel (){
	       
	    }
	    public BJPanel (BufferedImage bi){
	        this.bi = bi;
	        setPreferredSize(new Dimension(bi.getWidth(),bi.getHeight())) ;
	      }
	      
	      public void setBufferedImage(BufferedImage bi)
	      {
	        this.bi = bi;
	        setPreferredSize(new Dimension(bi.getWidth(),bi.getHeight())) ;
	        this.repaint();
	      }
	      
	      public void paintComponent(Graphics g)
	      {
	        g.drawImage(bi,0,0,this) ;
	      }
	  }
	
	//launch the application
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StoreData frame = new StoreData();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//create a frame
	public StoreData() {
		con = connection.createCon();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 441);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblACC_ID = new JLabel("ACC_ID");
		lblACC_ID.setBounds(10, 4, 80, 27);
		contentPane.add(lblACC_ID);
		
		txtACC_ID = new JTextField();
		txtACC_ID.setBounds(143, 7, 281, 20);
		
		contentPane.add(txtACC_ID);
		txtACC_ID.setColumns(10);
		
		JLabel lblACC_TYPE = new JLabel("ACC_TYPE");
		lblACC_TYPE.setBounds(10, 31, 80, 27);
		contentPane.add(lblACC_TYPE);
		
		txtACC_TYPE = new JTextField();
		txtACC_TYPE.setColumns(10);
		txtACC_TYPE.setBounds(143, 34, 281, 20);
		contentPane.add(txtACC_TYPE);
		
		JLabel lblC_ID = new JLabel("C_ID");
		lblC_ID.setBounds(10, 55, 80, 27);
		contentPane.add(lblC_ID);
		
		txtC_ID = new JTextField();
		txtC_ID.setColumns(10);
		txtC_ID.setBounds(143, 58, 281, 20);
		contentPane.add(txtC_ID);
		
		JLabel lblBALANCE = new JLabel("BALANCE");
		lblBALANCE.setBounds(10, 79, 80, 27);
		contentPane.add(lblBALANCE);
		
		txtBALANCE = new JTextField();
		txtBALANCE.setColumns(10);
		txtBALANCE.setBounds(143, 82, 281, 20);
		contentPane.add(txtBALANCE);
		
		JLabel lblCARD_NUMBER = new JLabel("CARD_NUMBER");
		lblCARD_NUMBER.setBounds(10, 109, 80, 27);
		contentPane.add(lblCARD_NUMBER);
		
		txtCARD_NUMBER = new JTextField();
		txtCARD_NUMBER.setColumns(10);
		txtCARD_NUMBER.setBounds(143, 112, 281, 20);
		contentPane.add(txtCARD_NUMBER);
		
		JLabel lblPIN_NO = new JLabel("PIN_NO");
		lblPIN_NO.setBounds(9, 137, 80, 27);
		contentPane.add(lblPIN_NO);
		
		btnFINGER = new JButton("FINGERPRINT");
		btnFINGER.setBounds(10, 170, 112, 27);
		btnFINGER.addActionListener(this);
		
		txtPIN_NO = new JTextField();
		txtPIN_NO.setColumns(10);
		txtPIN_NO.setBounds(142, 140, 281, 20);
		contentPane.add(txtPIN_NO);
		contentPane.add(btnFINGER);
		
		btncancel = new JButton("Cancel");
		btncancel.setBounds(177, 357, 89, 23);
		btncancel.addActionListener(this);
		
		lbl_fingerprint_image = new BJPanel();
//		lbl_fingerprint_image.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_fingerprint_image.setBounds(143, 176, 146, 139);
		contentPane.add(lbl_fingerprint_image);
		
		txtfinger = new JTextField();
		txtfinger.setEditable(false);
		txtfinger.setColumns(10);
		txtfinger.setBounds(10, 326, 281, 20);
		contentPane.add(txtfinger);
		
		btnsave = new JButton("SAVE");
		btnsave.setBounds(74, 357, 89, 23);
		btnsave.addActionListener(this);
		contentPane.add(btnsave);
		contentPane.add(btncancel);
		
		btnUpdate = new JButton("UPDATE");
		btnUpdate.setBounds(291, 357, 89, 23);
		btnUpdate.addActionListener(this);
		contentPane.add(btnUpdate);
	}
	
	public void initialisedevice() {
		mfs100 = new MFS100(this);
		ret = mfs100.Init();
		if (ret == 0) {
			deviceInfo= mfs100.GetDeviceInfo();
			if (deviceInfo != null) 
			{    
//				lbl_fingerprint_image.setText("Please put your finger in device"); 
				} 
			else 
			{ 
//				lbl_fingerprint_image.setText("Device Initialisation Problem"); 
			} 
			
			if(!(mfs100.GetErrorMsg(ret).toString()).equals("")){
//				System.out.println("error : "+mfs100.GetErrorMsg(ret));
				JOptionPane.showMessageDialog(contentPane, mfs100.GetErrorMsg(ret).toString());
			}
			
		}
	}

	public void startCapturing() {
		int quality = 55;
		int timeout = 0; //time in milli sec  ..default is 10 ..  0 = no limit
		int ret = mfs100.StartCapture(quality, timeout, true); 
		if (ret != 0) 
		{ 
//			ShowMessage(mfs100.GetErrorMsg(ret)); 
//			lbl_fingerprint_image.setText(mfs100.GetErrorMsg(ret));
		}
	}
	
	public void stopCapturing() {
		int ret = mfs100.StopCapture(); 
		if(ret == 0) {
//			JOptionPane.showMessageDialog(contentPane, "stopped");
		}
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
		//pressing fingerprint button
		if(e.getSource() == btnFINGER ) {			
			initialisedevice();
			startCapturing();	
		}
		
		//pressing cancel button
		else if(e.getSource()== btncancel) {
			acc_id="";
			acc_type="";
			c_id="";
			balance="";
			card_no="";
			pin_no="";
			fingerprint="";
			
			txtACC_ID.setText(acc_id);
			txtACC_TYPE.setText(acc_type);
			txtC_ID.setText(c_id);
			txtBALANCE.setText(balance);
			txtCARD_NUMBER.setText(card_no);
			txtPIN_NO.setText(pin_no);
			txtfinger.setText(fingerprint);		
		}
		
		//pressing save button
		else if(e.getSource()== btnsave) 
			{
				acc_id = txtACC_ID.getText().toString();
				acc_type = txtACC_TYPE.getText().toString();
				c_id = txtC_ID.getText().toString();
				balance = txtBALANCE.getText().toString();
				card_no = txtCARD_NUMBER.getText().toString();
				pin_no = txtPIN_NO.getText().toString();
				fingerprint = txtfinger.getText().toString();
					
					if(c_id.length()==0)
					{
						JOptionPane.showMessageDialog(contentPane, "Please Enter Your Customer ID");
					}
					else if(card_no.length() != 16) {
					JOptionPane.showMessageDialog(contentPane, "Please Enter 10 digit card number");
					}
					else if(pin_no.length() != 4)
					{
						JOptionPane.showMessageDialog(contentPane, "Please Enter 4 digit pin number");
					}
					else if(fingerprint.equals("")) {
						JOptionPane.showMessageDialog(contentPane, "Please provide atleast one fingerprint");
					}
					else if(fingerprint_image == null) {
						JOptionPane.showMessageDialog(contentPane, "Please provide atleast one fingerprint");
					}
					else
					{
						fingerprint = "C:\\Users\\milan\\Documents\\Device\\"+"image_"+c_id+".jpg";
						File outputfile = new File(fingerprint);
						try {
							ImageIO.write(icon,"jpg",outputfile);
//							JOptionPane.showMessageDialog(contentPane, "success");
							
						} catch (IOException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}
						
						HashMap<String, String> map = new HashMap();
						map.put("f1", acc_id);
						map.put("f2", acc_type);
						map.put("f3", c_id);
						map.put("f4", balance);
						map.put("f5", card_no);
						map.put("f6", pin_no);
						map.put("f7",fingerprint);
						String value = connection.insert(map,fingerprint_image);	
						JOptionPane.showMessageDialog(contentPane,value);
					}
			}
		
		//pressing update button
		else if(e.getSource() == btnUpdate) 
			{
				acc_id = txtACC_ID.getText().toString();
//				acc_id="0";
//				acc_type = txtACC_TYPE.getText().toString();
//				c_id = txtC_ID.getText().toString();
				c_id = "0";
//				balance = txtBALANCE.getText().toString();
				balance="0";
//				card_no = txtCARD_NUMBER.getText().toString();
				card_no="0";
//				pin_no = txtPIN_NO.getText().toString();
				pin_no="0";
//				fingerprint = txtfinger.getText().toString();
				
				fingerprint = "C:\\Users\\milan\\Documents\\Device\\"+"image_"+acc_id+"_update1.jpg";
				System.out.println("deko "+fingerprint);
				File outputfile = new File(fingerprint);
				try {
					ImageIO.write(icon,"jpg",outputfile);
//					JOptionPane.showMessageDialog(contentPane, "success");
					
				} catch (IOException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}
				
				if(acc_id.length()==0)
				{
					JOptionPane.showMessageDialog(contentPane, "Please Enter Your Account ID");
				}
//				else if(card_no.length() != 16) {
//				JOptionPane.showMessageDialog(contentPane, "Please Enter 10 digit card number");
//				}
//				else if(pin_no.length() != 4)
//				{
//					JOptionPane.showMessageDialog(contentPane, "Please Enter 4 digit pin number");
//				}
				else if(fingerprint.equals("")) {
					JOptionPane.showMessageDialog(contentPane, "Please provide atleast one fingerprint");
				}
				else
				{
					
					
					HashMap<String, String> map = new HashMap();
					map.put("f1", acc_id);
					map.put("f2", acc_type);
					map.put("f3", c_id);
					map.put("f4", balance);
					map.put("f5", card_no);
					map.put("f6", pin_no);
					map.put("f7",fingerprint);
					String value = connection.update(map,fingerprint_image);	
					JOptionPane.showMessageDialog(contentPane,value);
				}
				
			}
		}
		catch (Exception exx) {
//			System.out.println(exx);
		}
	}

	@Override
	public void OnCaptureCompleted(boolean arg0, int arg1, String arg2, FingerData arg3) {
		// TODO Auto-generated method stub
		stopCapturing();
		
		icon = mfs100.BytesToBitmap(arg3.FingerImage());
		
		
		lbl_fingerprint_image.getGraphics().drawImage(icon, 0, 0, lbl_fingerprint_image.getWidth(), lbl_fingerprint_image.getHeight(), null);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write( icon, "jpg", baos );
			baos.flush();
			fingerprint_image = baos.toByteArray();
			baos.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
//		m_finger1.setFingerPrintImage(icon) ;
//		m_finger1.ThinningHilditch();
//		m_finger1.ThinningHitAndMiss();
//		
//	    finger1 = m_finger1.getFingerPrintTemplate();
//	    
//	    m_bimage1 = m_finger1.getFingerPrintImageDetail();
////	    m_bimage1 = m_finger1.getFingerPrintImage();
//	    lbl_fingerprint_image.setBufferedImage(m_bimage1);
		
		
		
	}

	@Override
	public void OnPreview(FingerData arg0) {
		// TODO Auto-generated method stub
		icon = mfs100.BytesToBitmap(arg0.FingerImage());
		lbl_fingerprint_image.getGraphics().drawImage(icon, 0, 0, lbl_fingerprint_image.getWidth(), lbl_fingerprint_image.getHeight(), null);
		
	}
	
}
