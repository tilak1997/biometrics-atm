package GUI;


import Algorithm.*;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import MFS100.DeviceInfo;
import MFS100.FingerData;
import MFS100.MFS100;
import MFS100.MFS100Event;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.sql.*;

public class Index extends JFrame implements ActionListener,MFS100Event{
	
	private int acceptance_threshold = 30;
	
	protected static final Object NULL = null;
	//Helper Class objects
	ConnectionClass connection = new ConnectionClass();
	Connection con = connection.createCon();
	Utilities utility = new Utilities();
	Border emptyBorder = BorderFactory.createEmptyBorder();
	Date d = new Date();

	//Panels for different pages
	public JPanel contentPane,headerpanel;
	public JPanel panelWelcomeScreen,panelCardOptions;
	public JPanel panelCard_NO,panelCard_YES;
	public JPanel panelPinCode;
	public JPanel panelFingerprint;
	public JPanel panelFinalScreen;
	
	// label to put background image in respective pages
	public JLabel bgWelcomeScreen,bgCardOptions,bgCard_NO,bgCard_YES,bgPinCode,bgFingerprint;
	//ImageIcon variables
	public ImageIcon imgmain,scaledimgmain,fingerimage;
	
	public JLabel lblBankName =  new JLabel("SMTS Bank");
	public JLabel lblCurrentTime = new JLabel("");
	public JLabel lblCurrentDate = new JLabel("");
	
	public JPasswordField lblcardnumber;
	public JPasswordField lblpincode;
	public JLabel lblMessage;
	
	//values used during program
	static String directory = System.getProperty("user.dir").toString();
	Color fontColor = Color.WHITE;
	String time="", date="";
	String currentPage ="";
	String tempValue ="";
	String card_number="";
	String pin_code="";
	String probefilename="";
	boolean account_fingerprint=false;
	boolean authentication= false;
	boolean btncreated=false;
	int countFingerAttempt=0;
	int countPinAttempt=0;
	
	//number buttons
	public JButton btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0,btnplus,btnasterick;
	//options buttons
	public JButton btncancel,btnclear,btnaccept,btnback;
	//choose fingerprint button
	public JLabel btnFingerprint_image = new JLabel("Your FingerPrint Here");;  //this is actually JButton in previos project code
	
	
	//fingerprint template and feature holders
	CFingerPrint m_finger1 = new CFingerPrint();
	BufferedImage m_bimage1 = new BufferedImage(m_finger1.FP_IMAGE_WIDTH ,m_finger1.FP_IMAGE_HEIGHT,BufferedImage.TYPE_INT_RGB );
	double finger1[] = new double[m_finger1.FP_TEMPLATE_MAX_SIZE];
	
	//fingerprint scanner classes.
	MFS100 mfs100= null; 
	String Key = "";
	DeviceInfo deviceInfo = null;
	int ret;

	// Launch the application.
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {					
					Index frame = new Index();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
		
	//Create the frame.
	public Index() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 0, 0);
		setSize(1366,768);
		setResizable(false);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0,0,0,0));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		btnFingerprint_image.setBounds(515, 151, 332, 319);
		
		headerpanel = new JPanel();
		headerpanel.setBounds(0, 0, 1362, 120);
		headerpanel.setBackground(Color.BLACK);
		headerpanel.setLayout(null);
		contentPane.add(headerpanel);

		setHeaderPanel(headerpanel,fontColor);
		
		//updating time and date dynamically
		setTimer();
		//display first page
		showFirstPage();	
	}
	
	//method to set bank name , date and time in header panel
	public void setHeaderPanel(JPanel headerpanel,Color color) {
			lblBankName.setBounds(10, 50, 161, 31);
			labelStyle(lblBankName);
			headerpanel.add(lblBankName);

			lblCurrentTime.setBounds(1188, 50, 160, 31);
			labelStyle(lblCurrentTime);
			headerpanel.add(lblCurrentTime);

			lblCurrentDate.setBounds(592, 50, 177, 31);
			labelStyle(lblCurrentDate);
			headerpanel.add(lblCurrentDate);
		}
	
	//method to update time and date dynamically using timer task
	public void setTimer() {
		try {
			new Timer().scheduleAtFixedRate(new NextTask(), 0, 1000);
			}
			catch(Exception e) {
				System.out.println(e);
			}
	}
	
	//timer task class
	class NextTask extends TimerTask{
		NextTask(){
		}
		@Override
		public void run() {
			d = new Date();
			time = utility.showTime(d);
			date = utility.showDate(d);
			
			lblCurrentTime.setText(time);
			lblCurrentDate.setText(date);
		}	
	}
	
	// welcome screen
	public void showFirstPage() {
		
		try {			
			currentPage ="panelWelcomeScreen";
			
			panelWelcomeScreen = new JPanel();
			panelWelcomeScreen.setBounds(0, 120, 1362, 621);
			labelStyle(panelWelcomeScreen);
			
			imgmain = new ImageIcon(directory+"\\images\\background\\Screen 1.png");
			scaledimgmain= (ImageIcon) utility.ImageIconToIcon(imgmain,1362,621);
			
			JButton page1_insert_your_card = new JButton("");
			page1_insert_your_card.setIcon(new ImageIcon(directory+"\\images\\contents\\proceed.jpg"));
			page1_insert_your_card.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					showSecondPage();
				}
			});
			page1_insert_your_card.setBounds(457, 367, 448, 85);
			panelWelcomeScreen.add(page1_insert_your_card);
			
			bgWelcomeScreen = new JLabel("");
			bgWelcomeScreen.setBounds(0, 0, 1362, 621);
			panelWelcomeScreen.add(bgWelcomeScreen);
			bgWelcomeScreen.setIcon(scaledimgmain);	
			
			panelWelcomeScreen.setVisible(true);		
			
		}
		catch (Exception e) {
		}
	}

	// do you have ATM CARD ?
	public void showSecondPage() {
		panelWelcomeScreen.setVisible(false);			
		currentPage ="panelCardOptions";
		
		panelCardOptions = new JPanel();
		panelCardOptions.setBounds(0, 120, 1362, 621);
		labelStyle(panelCardOptions);
		
		imgmain = new ImageIcon(directory+"\\images\\background\\Screen 2.png");
		scaledimgmain= (ImageIcon) utility.ImageIconToIcon(imgmain,1362,621);
		
		JButton page2_yes = new JButton("");
		page2_yes.setBackground(Color.WHITE);
		page2_yes.setBorder(emptyBorder);
		page2_yes.setIcon(new ImageIcon(directory+"\\images\\contents\\yes.jpg"));
		page2_yes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showPageReadingCard();
			}
		});
		page2_yes.setBounds(546, 282, 280, 63);
		panelCardOptions.add(page2_yes);
		
		JButton page2_no = new JButton("");
		page2_no.setBackground(Color.WHITE);
		page2_no.setBorder(emptyBorder);
		page2_no.setIcon(new ImageIcon(directory+"\\images\\contents\\no.jpg"));
		page2_no.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showPageCardNUmber();
			}
		});
		page2_no.setBounds(546, 366, 280, 63);
		panelCardOptions.add(page2_no);
		
		btnback = new JButton("");
		btnback.setIcon(new ImageIcon(directory+"\\images\\contents\\back.jpg"));
		btnback.setBounds(10, 550, 82, 47);
		btnback.addActionListener(this);
		panelCardOptions.add(btnback);
		
		bgCardOptions = new JLabel("");
		bgCardOptions.setBounds(0, 0, 1362, 621);
		panelCardOptions.add(bgCardOptions);
		bgCardOptions.setIcon(scaledimgmain);

	}
	
	// do you have card ? -> NO
	public void showPageCardNUmber() {
		panelCardOptions.setVisible(false);
		currentPage ="panelCard_NO";
		
		panelCard_NO = new JPanel();
		panelCard_NO.setBounds(0, 120, 1362, 621);
		labelStyle(panelCard_NO);
		
		imgmain = new ImageIcon(directory+"\\images\\background\\Screen 3.png");
		scaledimgmain= (ImageIcon) utility.ImageIconToIcon(imgmain,1362,621);
		
		lblcardnumber = new JPasswordField(card_number);
		lblcardnumber.setBounds(876, 53, 343, 95);
		labelStyle(lblcardnumber);
		
		panelCard_NO.add(lblcardnumber);
		
		if(!btncreated)
		{
		createButtons();
		}
		
		showButtons(panelCard_NO);
		
		panelCard_NO.add(btnback);
		
		lblMessage = new JLabel("");
		labelStyle(lblMessage);
		lblMessage.setForeground(Color.RED);
		lblMessage.setBounds(128, 189, 633, 28);
		panelCard_NO.add(lblMessage);
		
		bgCard_NO = new JLabel("");
		bgCard_NO.setBounds(0, 0, 1362, 621);
		panelCard_NO.add(bgCard_NO);
		bgCard_NO.setIcon(scaledimgmain);	
	
}
	
	//do you have card ? -> YES
	public void showPageReadingCard() {
		panelCardOptions.setVisible(false);
		currentPage ="panelCard_YES";
		
		panelCard_YES = new JPanel();
		panelCard_YES.setBounds(0, 120, 1362, 621);
		labelStyle(panelCard_YES);
		
		imgmain = new ImageIcon(directory+"\\images\\background\\Screen 3-2.png");
		scaledimgmain= (ImageIcon) utility.ImageIconToIcon(imgmain,1362,621);
		
		panelCard_YES.add(btnback);
		
		bgCard_YES = new JLabel("");
		bgCard_YES.setBounds(0, 0, 1362, 621);
		panelCard_YES.add(bgCard_YES);
		bgCard_YES.setIcon(scaledimgmain);
	}
	
	//page to enter your pin code
	public void showPagePinCode() {
			if(currentPage=="panelCard_YES") {
				panelCard_YES.setVisible(false);
			}
			else if(currentPage=="panelCard_NO")
			{
				panelCard_NO.setVisible(false);
			}
			else if(currentPage=="finalpage") {
				panelFinalScreen.setVisible(false);
			}
			
			currentPage ="panelPinCode";
			
			panelPinCode = new JPanel();
			panelPinCode.setBounds(0, 120, 1362, 621);
			labelStyle(panelPinCode);
			
			imgmain = new ImageIcon(directory+"\\images\\background\\Screen 4.png");
			scaledimgmain= (ImageIcon) utility.ImageIconToIcon(imgmain,1362,621);
			
			lblpincode = new JPasswordField(pin_code);
			lblpincode.setBounds(876, 54, 343, 94);
			labelStyle(lblpincode);
			panelPinCode.add(lblpincode);
			
			if(!btncreated) {
			createButtons();
			}
			showButtons(panelPinCode);
			
			panelPinCode.add(btnback);
			
			panelPinCode.add(lblMessage);
			
			bgPinCode = new JLabel("");
			bgPinCode.setBounds(0, 0, 1362, 621);
			panelPinCode.add(bgPinCode);
			bgPinCode.setIcon(scaledimgmain);
		
	}
	
	//page to provide your fingerprint
	public void showPageFingerPrint() {	
		
		//initialise fingerprint device
//		initialiseDevice();
		
		//start capturing fingerprint
//		startCapturingFingerprint();	
		
		if(currentPage=="panelPinCode") {
			panelPinCode.setVisible(false);
		}
			
			currentPage ="panelFingerprint";
			
			imgmain = new ImageIcon(directory+"\\images\\background\\Screen 4-2.png");
			scaledimgmain= (ImageIcon) utility.ImageIconToIcon(imgmain,1362,621);
			
			panelFingerprint = new JPanel();
			panelFingerprint.setBounds(0, 120, 1362, 621);
			labelStyle(panelFingerprint);
			
			
			panelFingerprint.add(btnFingerprint_image);				
			
			JButton btnNext = new JButton("");
			btnNext.setBounds(1230, 550, 104, 47);
			btnNext.setIcon(new ImageIcon(directory+"\\images\\contents\\accept.jpg"));
			btnNext.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					stopCapturing();
					
					if (authentication== true)
					{
						
						showfinalpage();
					}
					else {
						JOptionPane.showMessageDialog(panelFingerprint, "Fingerprint not verified .. please try again");
						initialiseDevice();
						startCapturingFingerprint();
					}
					
				}
			});
			panelFingerprint.add(btnNext);
			
			panelFingerprint.add(btnback);
			
			bgFingerprint = new JLabel("");
			bgFingerprint.setBounds(0, 0, 1362, 621);
			panelFingerprint.add(bgFingerprint);
			bgFingerprint.setIcon(scaledimgmain);
			
	}
	
	//final page
	public void showfinalpage() {
		if(currentPage=="panelFingerprint") {
		panelFingerprint.setVisible(false);
		}
		else {
			panelPinCode.setVisible(false);
		}
		
		currentPage ="finalpage";
		
		panelFinalScreen = new JPanel();
		panelFinalScreen.setBounds(0, 120, 1362, 621);
		labelStyle(panelFinalScreen);
		
		imgmain = new ImageIcon("E:\\0) ATM\\Blank\\Screen 6.png");
		scaledimgmain= (ImageIcon) utility.ImageIconToIcon(imgmain,1362,621);
		
		panelFinalScreen.add(btnback);
		
		JLabel lblNewLabel = new JLabel("Fast Cash");
		labelStyle(lblNewLabel);
		lblNewLabel.setBounds(0, 86, 292, 114);
		panelFinalScreen.add(lblNewLabel);
		
		JLabel label = new JLabel("Balance Enquiry");
		labelStyle(label);
		label.setBounds(0, 248, 292, 114);
		panelFinalScreen.add(label);
		
		JLabel label_1 = new JLabel("Mini Statement");
		labelStyle(label_1);
		label_1.setBounds(0, 407, 292, 114);
		panelFinalScreen.add(label_1);
		
		JLabel label_2 = new JLabel("Cash Withdrawal");
		labelStyle(label_2);
		label_2.setBounds(1070, 86, 292, 114);
		panelFinalScreen.add(label_2);
		
		JLabel label_3 = new JLabel("Change Pin");
		labelStyle(label_3);
		label_3.setBounds(1070, 248, 292, 114);
		panelFinalScreen.add(label_3);
		
		JLabel label_4 = new JLabel("Transfer");
		labelStyle(label_4);
		label_4.setBounds(1070, 407, 292, 114);
		panelFinalScreen.add(label_4);
		
		JButton btnEnd = new JButton("");
		btnEnd.setBounds(1230, 550, 104, 47);
		btnEnd.setIcon(new ImageIcon(directory+"\\images\\contents\\cancel.jpg"));
		btnEnd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {		
				cancelTransaction(panelFinalScreen);			
			}
		});
		panelFinalScreen.add(btnEnd);
		
		bgCard_YES = new JLabel("");
		bgCard_YES.setBounds(0, 0, 1362, 621);
		panelFinalScreen.add(bgCard_YES);
		bgCard_YES.setIcon(scaledimgmain);	
	}
	
	// JLabels styling
	public void labelStyle(JLabel button) {
		button.setHorizontalAlignment(SwingConstants.CENTER);
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Times New Roman", Font.BOLD, 28));
	}
	
	// JButton style
	public void labelStyle(JButton button) {
		button.setHorizontalAlignment(SwingConstants.CENTER);
		button.setForeground(Color.BLACK);
		button.setFont(new Font("Times New Roman", Font.BOLD, 28));
		button.addActionListener(this);
	}
	
	// JPassword fied style
	public void labelStyle(JPasswordField field) {
		field.setHorizontalAlignment(SwingConstants.CENTER);
		field.setFont(new Font("Rockwell Condensed", Font.BOLD, 28));
		field.setBackground(null);
		field.setForeground(Color.BLACK);
		field.setEchoChar('*');
		field.setEditable(false);
		
	}
	
	// Jpanel style
	public void labelStyle(JPanel panel) {
		panel.setLayout(null);
		contentPane.add(panel);
		panel.setVisible(true);
	}
	
	//cancel the transaction
	public void cancelTransaction(JPanel panel) {
		countFingerAttempt=0;
		card_number="";
		pin_code="";
		probefilename="";
		authentication = false;
		connection.close();
		con = new ConnectionClass().createCon();
		panel.setVisible(false);
		showFirstPage();
		stopCapturing();
	}
	
	//method to create buttons
	public void createButtons(){
		btn1 = new JButton("1");
		labelStyle(btn1);
		btn1.setBounds(876, 170, 106, 68);
		
		btn2 = new JButton("2");
		labelStyle(btn2);
		btn2.setBounds(996, 170, 106, 68);
		
		btn3 = new JButton("3");
		labelStyle(btn3);
		btn3.setBounds(1113, 170, 106, 68);
		
		btn4 = new JButton("4");
		labelStyle(btn4);
		btn4.setBounds(876, 249, 106, 68);
		
		btn5 = new JButton("5");
		labelStyle(btn5);
		btn5.setBounds(996, 249, 106, 68);
		
		btn6 = new JButton("6");
		labelStyle(btn6);
		btn6.setBounds(1113, 249, 106, 68);
		
		btn7 = new JButton("7");
		labelStyle(btn7);
		btn7.setBounds(876, 328, 106, 68);
		
		btn8 = new JButton("8");
		labelStyle(btn8);
		btn8.setBounds(996, 328, 106, 68);
		
		btn9 = new JButton("9");
		labelStyle(btn9);
		btn9.setBounds(1113, 328, 106, 68);
		
		btn0 = new JButton("0");
		labelStyle(btn0);
		btn0.setBounds(996, 417, 106, 68);
		
		btnplus = new JButton("+");
		labelStyle(btnplus);
		btnplus.setBounds(1113, 417, 106, 68);
		
		btnasterick = new JButton("*");
		labelStyle(btnasterick);
		btnasterick.setBounds(876, 417, 106, 68);
		
		btnaccept = new JButton("");
		btnaccept.setIcon(new ImageIcon(directory+"\\images\\contents\\accept.jpg"));
		btnaccept.setBounds(1113, 498, 106, 68);
		btnaccept.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(currentPage.equals("panelCard_NO")) {
					if(card_number.length() != 16) {
						lblMessage.setForeground(Color.RED);
						lblMessage.setText("Please enter 16 digit card number");
					}
					else {
						if((connection.checkcard(con,card_number)) == true) {
							lblMessage.setForeground(Color.GREEN);
							lblMessage.setText(" Card Verified");
							showPagePinCode();
						}
						else {
							lblMessage.setForeground(Color.RED);
							lblMessage.setText("Invalid Card Number");
						}		
					}
					card_number = "";
					lblcardnumber.setText(card_number);
				}
				
				else if(currentPage.equals("panelPinCode")){
					if(countPinAttempt<3) {

						if(pin_code.length() != 4) {
							lblMessage.setForeground(Color.RED);
							lblMessage.setText("Please Enter Valid 4 digit pin number");
							countPinAttempt++;
						}
						else
						{
							if(connection.checkpin(con,pin_code)) {
								countFingerAttempt = 0;
								lblMessage.setForeground(Color.GREEN);
								lblMessage.setText("Pin Verified");
								
								if(connection.length < 1) {
									account_fingerprint=false;
									showfinalpage();
								}
								else {
									account_fingerprint=true;
									initialiseDevice();
									startCapturingFingerprint();
									showPageFingerPrint();
								}
								countPinAttempt = 0;
							}
							else
							{
								lblMessage.setForeground(Color.RED);
								lblMessage.setText("Pin Code Not Matched");
								countPinAttempt++;
							}	
						}
						pin_code="";
						lblpincode.setText(pin_code);
					}
					else {
						JOptionPane.showMessageDialog(panelPinCode,"Too Many Pin Attempts.. Redirecting to home page");
						countPinAttempt = 0;
						cancelTransaction(panelPinCode);
					}
				}
			}
		});
		
		btncancel = new JButton("");
		btncancel.setIcon(new ImageIcon(directory+"\\images\\contents\\cancel.jpg"));
		btncancel.setBounds(876, 498, 106, 68);
		btncancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
					card_number = "";
					pin_code = "";
					if(currentPage.equals("panelCard_NO")) {					
						panelCard_NO.setVisible(false);					
					}
					else if(currentPage.equals("panelPinCode")) {
						panelPinCode.setVisible(false);					
					}
					showSecondPage();	
			}
		});
		
		btnclear = new JButton("");
		btnclear.setIcon(new ImageIcon(directory+"\\images\\contents\\clear.jpg"));
		btnclear.setBounds(996, 498, 106, 68);
		btnclear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(currentPage.equals("panelCard_NO"))
				{
					card_number = "";
					lblcardnumber.setText(card_number);
				}
				else if(currentPage.equals("panelPinCode")){
					pin_code="";
					lblpincode.setText(pin_code);
				}	
			}
		});
		
		btncreated=true;
	}
	
	//method to display buttons in panel container
	public void showButtons(JPanel panel) {
		panel.add(btn1);
		panel.add(btn2);
		panel.add(btn3);
		panel.add(btn4);
		panel.add(btn5);
		panel.add(btn6);
		panel.add(btn7);
		panel.add(btn8);
		panel.add(btn9);
		panel.add(btn0);
		panel.add(btnplus);
		panel.add(btnasterick);
		panel.add(btnaccept);
		panel.add(btncancel);
		panel.add(btnclear);

}

	//button listener method
	@Override
	public void actionPerformed(ActionEvent e) {
		// set tempValues
		if(e.getSource() == btn1) {
			tempValue="1";
		} 
		else if(e.getSource() == btn2) {
			tempValue="2";
		}
		else if(e.getSource() == btn3) {
			tempValue="3";
		}
		else if(e.getSource() == btn4) {
			tempValue="4";
		}
		else if(e.getSource() == btn5) {
			tempValue="5";
		}
		else if(e.getSource() == btn6) {
			tempValue="6";
		}
		else if(e.getSource() == btn7) {
			tempValue="7";
		}
		else if(e.getSource() == btn8) {
			tempValue="8";
		}
		else if(e.getSource() == btn9) {
			tempValue="9";
		}
		else if(e.getSource() == btn0) {
			tempValue="0";
		}
		else if(e.getSource() == btnplus) {
			tempValue="+";
		}
		else if(e.getSource() == btnasterick) {
			tempValue="*";
		}  //end of set tempValues
		
		// add card number
		if(currentPage.equals("panelCard_NO")) {
			if(card_number.length() <16)
			{
			card_number += tempValue;
			lblcardnumber.setText(card_number);
			}
		} //end of add card number
		
		// add pin code
		if(currentPage.equals("panelPinCode")){
			if(pin_code.length() <4)
			{
			pin_code += tempValue;
			lblpincode.setText(pin_code);
			}
		} //end of add pin number
				
		//back button
		if(e.getSource() == btnback) 
		{
			 card_number ="";
			 pin_code ="";
			 tempValue ="";
			 
			if(currentPage.equals("panelCardOptions")) {
				panelCardOptions.setVisible(false);
				showFirstPage();
				}
			else if(currentPage.equals("panelCard_YES")) {
				panelCard_YES.setVisible(false);
				showSecondPage();
				
			}
			else if(currentPage.equals("panelCard_NO")) {
				panelCard_NO.setVisible(false);
				showSecondPage();
				
			}
			else if(currentPage.equals("panelPinCode")) {
				panelPinCode.setVisible(false);	
				showPageCardNUmber();
			}
			else if(currentPage.equals("panelFingerprint")) {
				panelFingerprint.setVisible(false);	
				lblMessage.setText("");
				stopCapturing();
				showPagePinCode();
			}
			else if(currentPage.equals("finalpage")) {
				panelFinalScreen.setVisible(false);
				authentication = false;
				if(account_fingerprint) {
					showPageFingerPrint();
					startCapturingFingerprint();
				}
				else {
					showPagePinCode();
				}
				
			}
		} //end of back button
				
	} // end of button listener method

	@Override
	public void OnCaptureCompleted(boolean arg0, int arg1, String arg2, FingerData arg3) {
		// TODO Auto-generated method stub
		BufferedImage icon = mfs100.BytesToBitmap(arg3.FingerImage());
		btnFingerprint_image.getGraphics().drawImage(icon, 0, 0, btnFingerprint_image.getWidth(), btnFingerprint_image.getHeight(), null);
		stopCapturing();
		comareFingerprintNew(icon);
		startCapturingFingerprint();
		
	}

	@Override
	public void OnPreview(FingerData arg0) {
		// TODO Auto-generated method stub
		BufferedImage icon = mfs100.BytesToBitmap(arg0.FingerImage());
				
	}
	
	public void initialiseDevice() {
		mfs100 = new MFS100(this);
		ret = mfs100.Init();
		if (ret == 0) {
			deviceInfo= mfs100.GetDeviceInfo();
			if (deviceInfo != null) 
			{    
//				String scannerInfo = "SERIAL NO.: "+ deviceInfo.SerialNo() + " MAKE: "+ deviceInfo.Make() + " MODEL: "+ deviceInfo.Model(); 
//				btnFingerprint_image.setText("Please put your finger in device"); 
				} 
			else 
			{ 
//				System.out.println("error : "+mfs100.GetErrorMsg(ret));
				JOptionPane.showMessageDialog(contentPane, mfs100.GetErrorMsg(ret).toString());
			} 
		}
	}
	
	public void startCapturingFingerprint(){
		int quality = 60;
		int timeout = 0; //time in milli sec  ..default is 10 ..  0 = no limit
		int start = mfs100.StartCapture(quality, timeout, true); 
		if (start != 0) 
		{ 
			JOptionPane.showMessageDialog(contentPane, mfs100.GetErrorMsg(ret).toString());
		}
		
		
	}
	
	public void stopCapturing() {
		int stop = mfs100.StopCapture(); 
		if(stop == 0) {
//			JOptionPane.showMessageDialog(contentPane, "stopped");
		}
	}
	
	public void compareFingerprintOld() {
		try {
			if(countFingerAttempt < attempts) {
				JFileChooser jfc = new JFileChooser();
				jfc.showOpenDialog(panelFingerprint);
				probefilename = jfc.getSelectedFile().toString();
				
				if(!(probefilename.equals(""))) {	
						m_bimage1=ImageIO.read(new File(probefilename));

						m_finger1.setFingerPrintImage(m_bimage1);
						finger1 = m_finger1.getFingerPrintTemplate();
													
//						Double value = connection.checking(probefilename);  //check by using finger print location
						Double value = connection.checking(finger1);  //check by using finger print template
						
						if(value>acceptance_threshold) {
							authentication = true;
							countFingerAttempt = 0;
						}
						else {
							authentication = false;
							countFingerAttempt++;
						}
						
						if(authentication == true) {
							showfinalpage();
						}
						else {
							JOptionPane.showMessageDialog (panelFingerprint,"Please try again !");
						}
						
//						JOptionPane.showMessageDialog (panelFingerprint,value+" % Match "); 
						
					} //if not empty file/fingerprint
					
				} //if fingerprint attempt less than 10
			
			else {
					JOptionPane.showMessageDialog (panelFingerprint,"Too Many Attemps. Redirecting you to Home Page"); 
					cancelTransaction(panelFingerprint);
				} //else fingerprint too many attempts					
			
		}
		catch (Exception ee) {
			// TODO: handle exception
			JOptionPane.showMessageDialog (panelFingerprint,"Error Message :"+ee.getMessage()); 
		}
	}
	
	int attempts = 10;
	
	public void comareFingerprintNew(BufferedImage image) {
		try 
		{
			if(countFingerAttempt < attempts) 
			{
				m_finger1.setFingerPrintImage(image);
				m_finger1.ThinningHilditch();
				m_finger1.ThinningHitAndMiss();
				
				finger1=m_finger1.getFingerPrintTemplate();
				
//				System.out.println(finger1[0]);
				
				Double value = connection.checking(finger1);  //check by using finger print template
				
				if(value>35) {
					authentication = true;
					countFingerAttempt = 0; 
				}
				else {
					authentication = false;
					countFingerAttempt++;
					
				}
				
				if(authentication == true) {
					stopCapturing();
					showfinalpage();
				}
				else {
					JOptionPane.showMessageDialog (panelFingerprint,"Please try again !");
				}
//				JOptionPane.showMessageDialog (panelFingerprint,value+" % Match ");	
			}
			else {
				stopCapturing();
				JOptionPane.showMessageDialog (panelFingerprint,"Too Many Attemps. Redirecting you to Home Page"); 
				cancelTransaction(panelFingerprint);
			}
		}
		catch (Exception ee) {
			// TODO: handle exception
			JOptionPane.showMessageDialog (panelFingerprint,"Error Message :"+ee.getMessage()); 
		}
		
	}
}
