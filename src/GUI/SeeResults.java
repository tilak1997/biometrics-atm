package GUI;

import Algorithm.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import MFS100.DeviceInfo;
import MFS100.FingerData;
import MFS100.MFS100;
import MFS100.MFS100Event;

public class SeeResults extends JFrame implements ActionListener,MFS100Event {

	private JPanel contentPane;
	
	private CFingerPrint m_finger1 = new CFingerPrint();
	
	private BJPanel m_panel1 = new BJPanel();
	private BJPanel m_panel2 = new BJPanel();
	private BufferedImage m_bimage1 = new BufferedImage(m_finger1.FP_IMAGE_WIDTH ,m_finger1.FP_IMAGE_HEIGHT,BufferedImage.TYPE_INT_RGB );
	double finger1[] = new double[m_finger1.FP_TEMPLATE_MAX_SIZE];
	
	private JButton btn1;
	String image1path="",image2path="";
	
	MFS100 mfs100= null; 
	String Key = "";
	DeviceInfo deviceInfo = null;
	int ret;
	
	
	BufferedImage icon = new BufferedImage(m_finger1.FP_IMAGE_WIDTH ,m_finger1.FP_IMAGE_HEIGHT,BufferedImage.TYPE_INT_RGB );

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SeeResults frame = new SeeResults();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SeeResults() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0,0,850,500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLayout(null);
		start();
	}
	
	public void start() {
		initialisedevice();
		
		btn1 = new JButton("SELECT IMAGE");
		
		btn1.addActionListener(this);
		
		m_panel1.setBounds(0, 0, 323, 352);
		m_panel2.setBounds(500, 0, 323, 352);
		
		btn1.setBounds(0, 380, 100, 50);

		getContentPane().add(m_panel1);
		getContentPane().add(m_panel2);
		
		getContentPane().add(btn1);
		
	}
	
	class BJPanel extends JPanel 
	  {
	    public BufferedImage bi;
	    public BJPanel (){
	       
	    }
	    public BJPanel (BufferedImage bi){
	        this.bi = bi;
	        setPreferredSize(new Dimension(bi.getWidth(),bi.getHeight())) ;
	      }
	      
	      public void setBufferedImage(BufferedImage bi)
	      {
	        this.bi = bi;
	        setPreferredSize(new Dimension(bi.getWidth(),bi.getHeight())) ;
	        this.repaint();
	      }
	      
	      public void paintComponent(Graphics g)
	      {
	        g.drawImage(bi,0,0,this) ;
	      }
	  }

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn1) {
			try {
				startCapturing();
			}
			catch (Exception exx) {
				// TODO: handle exception
			}
		}
		
	}
	
	public void initialisedevice() {
		mfs100 = new MFS100(this);
		ret = mfs100.Init();
		if (ret == 0) {
			deviceInfo= mfs100.GetDeviceInfo();
			if (deviceInfo != null) 
			{    
//				lbl_fingerprint_image.setText("Please put your finger in device"); 
				} 
			else 
			{ 
//				lbl_fingerprint_image.setText("Device Initialisation Problem"); 
			} 
			
			if(!(mfs100.GetErrorMsg(ret).toString()).equals("")){
//				System.out.println("error : "+mfs100.GetErrorMsg(ret));
				JOptionPane.showMessageDialog(contentPane, mfs100.GetErrorMsg(ret).toString());
			}
			
		}
	}

	public void startCapturing() {
		int quality = 55;
		int timeout = 0; //time in milli sec  ..default is 10 ..  0 = no limit
		int ret = mfs100.StartCapture(quality, timeout, true); 
		if (ret != 0) 
		{ 
//			ShowMessage(mfs100.GetErrorMsg(ret)); 
//			lbl_fingerprint_image.setText(mfs100.GetErrorMsg(ret));
		}
	}
	
	public void stopCapturing() {
		int ret = mfs100.StopCapture(); 
		if(ret == 0) {
//			JOptionPane.showMessageDialog(contentPane, "stopped");
			JOptionPane.showMessageDialog(contentPane, mfs100.GetErrorMsg(ret).toString());
		}
	}

	@Override
	public void OnCaptureCompleted(boolean arg0, int arg1, String arg2, FingerData arg3) {
		// TODO Auto-generated method stub
		icon = mfs100.BytesToBitmap(arg3.FingerImage());
//		m_bimage1=ImageIO.read(icon) ;
		m_panel1.setBufferedImage(icon);
		
		m_finger1.setFingerPrintImage(icon) ;
		m_finger1.ThinningHilditch();
		m_finger1.ThinningHitAndMiss();
		
	    finger1 = m_finger1.getFingerPrintTemplate();
	    
	    m_bimage1 = m_finger1.getFingerPrintImageDetail();
//	    m_bimage1 = m_finger1.getFingerPrintImage();
	    m_panel2.setBufferedImage(m_bimage1);
		
	}

	@Override
	public void OnPreview(FingerData arg0) {
		// TODO Auto-generated method stub
		
	}

}
